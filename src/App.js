import React, { Component } from "react";
import ListItem from "./components/ListItem";
import Filters from "./components/Filters";

import "./sass/App.scss";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: null,
      genres: null,
      error: null,
      isLoading: false,
      selectedOption: []

    };
  }

  componentDidMount() {
    const apiKey = "99ed378148a4dbc4dddbddee5a7bf110";
    const apiRequest1 = fetch(
      `https://api.themoviedb.org/3/movie/now_playing?api_key=${apiKey}&language=en-US&page=1`
    ).then(response => response.json());
    const apiRequest2 = fetch(
      `https://api.themoviedb.org/3/genre/movie/list?api_key=${apiKey}&language=en-US`
    ).then(response => response.json());

    this.setState({ isLoading: true });

    Promise.all([apiRequest1, apiRequest2])
      .then(values =>
        this.setState({
          movies: values[0],
          genres: values[1],
          isLoading: false
        })
      )
      .catch(error => this.setState({ error }));
  }

  _handleChange = (e) => {
    const selectedOption = this.state.selectedOption;
    const options = selectedOption.concat(parseInt(e.target.id));
    this.setState({
        selectedOption: options
    });
  }

  render() {
    const { movies, isLoading, genres, selectedOption } = this.state;
    //const DataByPopularity = movies && movies.results.sort((a, b) => b.popular - a.popular);
    // Movies already sorted by popularity so didnt need to do it again

    const moviesWithGenre = movies && movies.results.map( item => {
      const genresArray = item.genre_ids.map( genre => {
        const matchingGenre = genres && genres.genres.find(genreItem => genreItem.id === genre)
        return matchingGenre.name
      })
      return {
        ...item,
        genre: genresArray
      }
    })

    const filterByGenre = movies && moviesWithGenre.filter( item => {
      let checker = (arr, target) => target.every(v => arr.includes(v));
        if (checker(item.genre_ids,selectedOption)) {
            return item
        }
    })
    
    return (
      <div id="app">
        <header>
          <h1>TMDB Movies</h1>
        </header>
        <main>
          {isLoading ? (
            <p>Loading ...</p>
          ) : (
            <div>
              <Filters
                genres={genres ? genres.genres : []}
                handleOptionChange={this._handleChange}
                selectedOption={selectedOption}
              />
              <ListItem data={selectedOption.length > 0 ? filterByGenre : moviesWithGenre} />
            </div>
          )}
        </main>
      </div>
    );
  }
}

export default App;
