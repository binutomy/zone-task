import React from 'react';
import PropTypes from 'prop-types';

const Filters = ({genres, handleOptionChange,selectedOption }) => {
        return (
            <form>
                <ul className='list--block'>
                {genres.map(({ id, name }) => (
                    <li className="list--item" key={id}>
                         <input type="checkbox" 
                            className="check--box"
                            value={name}
                            id={id}
                            checked={selectedOption.includes[id]}
                            onChange={handleOptionChange} /> 
                        <label className="check--box__label" htmlFor={name}>{name}</label>
                    </li>
                ))}
                </ul>
            </form>
        );
}

export default Filters;


Filters.propTypes = {
    genres: PropTypes.arrayOf(PropTypes.object),
    handleOptionChange : PropTypes.func,
    selectedOption : PropTypes.array
};