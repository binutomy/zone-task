import React from "react";
import PropTypes from 'prop-types';

const ListItem = ({data}) => {
    const movies = data && data.map( movie => {
                    return (
                    <li key={movie.id} className="card--item">
                       <img className="card__img" alt={movie.title} src={`https://image.tmdb.org/t/p/w400/${movie.poster_path}`} />
                        <div className="card__details">
                            <h4 className="card__title">{movie.title}</h4>
                            <p className="card__text">{movie.overview}</p>
                            <p>Genres : {movie.genre.map( item => 
                                <span key={item}> {item} </span> )}</p>
                        </div>
                    </li>
                    )
                });
    return (
        <ul className="card--wrapper">
            {movies}
        </ul>
        );
};
      
export default ListItem;


ListItem.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object)
};