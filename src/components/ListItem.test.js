import React from 'react'
import { shallow } from 'enzyme'
import ListItem from './ListItem';

describe('<ListItem />', () => {

    it('should render a list of Movies', () => {
        // Given
        const Movies = {
            data: [{
                    id: 1,
                    title: 'A'
                },
                {
                    id: 2,
                    title: 'B'
                }
            ]
        }
            
        const render = shallow(<ListItem movies={Movies} />);
        const listItems = render.find('li');

        expect(listItems).toHaveLength(2);
    });

});